/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const tableRouter = {
  path: '/system',
  component: Layout,
  redirect: '/system/Module',
  name: '系统管理',
  meta: {
    title: 'system',
    icon: 'table'
  },
  children: [
    {
      path: 'Module',
      component: () => import('@/views/system/module/index'),
      name: 'Module',
      meta: { title: 'Module' }
    },
    {
      path: 'AdminButton',
      component: () => import('@/views/system/AdminButton/index'),
      name: 'AdminButton',
      meta: { title: 'AdminButton' }
    }
  ]
}
export default tableRouter
