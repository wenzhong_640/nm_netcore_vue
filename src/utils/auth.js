import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const RolesKey = 'Admin-Roles'
const UserIDKey = 'Admin-UserID'
const UserNameKey = 'Admin-UserName'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getRoles() {
  return Cookies.get(RolesKey)
}

export function setRoles(Roles) {
  return Cookies.set(RolesKey, Roles)
}

export function removeRoles() {
  return Cookies.remove(RolesKey)
}

export function getUserID() {
  return Cookies.get(UserIDKey)
}

export function setUserID(UserID) {
  return Cookies.set(UserIDKey, UserID)
}

export function removeUserID() {
  return Cookies.remove(UserIDKey)
}

export function getUserName() {
  return Cookies.get(UserNameKey)
}

export function setUserName(UserNam) {
  return Cookies.set(UserNameKey, UserNam)
}

export function removeUserName() {
  return Cookies.remove(UserNameKey)
}
