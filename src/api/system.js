import request9527 from '@/utils/request9527'
// 返回模块列表
export function GetModule(data) {
  return request9527({
    url: `/Module/GetModule`,
    method: 'get'
  })
}
// 返回模块子列表
export function GetModuleChild(parentId) {
  return request9527({
    url: `/Module/GetModuleChild`,
    method: 'get',
    params: { parentId }
  })
}
// 添加模块
export function AddModule(data) {
  return request9527({
    url: '/Module/AddModule',
    method: 'post',
    data
  })
}
// 删除模块
export function DeleteModule(Id) {
  return request9527({
    url: `/Module/DeleteModule`,
    method: 'get',
    params: { Id }
  })
}
// 修改模块
export function EditModule(data) {
  return request9527({
    url: '/Module/EditModule',
    method: 'post',
    data
  })
}

// 获取所有按钮
export function GetBtns() {
  return request9527({
    url: '/Btn/GetBtns',
    method: 'get'
  })
}

// 添加按钮
export function AddBtn(data) {
  return request9527({
    url: '/Btn/AddBtn',
    method: 'post',
    data
  })
}
// 修改按钮
export function EditBtn(data) {
  return request9527({
    url: '/Btn/EditBtn',
    method: 'post',
    data
  })
}
// 删除按钮
export function DeleteBtn(btnId) {
  return request9527({
    url: `/Btn/DeleteBtn`,
    method: 'post',
    params: { btnId }
  })
}
// 获取所有模块按钮
export function GetModuleBtns() {
  return request9527({
    url: `/Btn/GetModuleBtns`,
    method: 'get'
  })
}
// 获取所有模块下的按钮信息
export function Module_GetModuleBtns() {
  return request9527({
    url: `Module/GetModuleBtns`,
    method: 'get'
  })
}
// 修改模块按钮
export function EditModuleBtn(data) {
  return request9527({
    url: `/Btn/EditModuleBtn`,
    method: 'post',
    data
  })
}
// 获取用户模块按钮
export function GetUserModuleBtns(data) {
  return request9527({
    url: `/User/GetUserModuleBtns`,
    method: 'get',
    params: { moduleId: data.moduleId, userId: data.userId }
  })
}
// 获取组织架构
export function GetDepts() {
  return request9527({
    url: `/Dept/GetDepts`,
    method: 'get'
  })
}
// 根据部门获取角色
export function GetRoleByDept(deptId) {
  return request9527({
    url: `/Role/GetRoleByDept`,
    method: 'get',
    params: { deptId }
  })
}
// 删除 角色
export function DeleteRole(Id) {
  return request9527({
    url: `/Role/DeleteRole`,
    method: 'get',
    params: { Id }
  })
}
// 修改角色 EditRole
export function EditRole(data) {
  return request9527({
    url: `/Role/EditRole`,
    method: 'post',
    data
  })
}
// 新建角色 AddRole
export function AddRole(data) {
  return request9527({
    url: `/Role/AddRole`,
    method: 'post',
    data
  })
}

// 查看角色 GetRole
export function GetRole(Id) {
  return request9527({
    url: `/Role/GetRole`,
    method: 'get',
    params: { Id }
  })
}

// 获取角色用户 GetRole
export function GetRoleUsers(Id) {
  return request9527({
    url: `/Role/GetRoleUsers`,
    method: 'get',
    params: { Id }
  })
}
// 获取部门用户 GetDeptUsers
export function GetDeptUsers(deptId) {
  return request9527({
    url: `/User/GetDeptUsers`,
    method: 'get',
    params: { deptId }
  })
}
// 修改角色用户
export function EditRoleUsers(data) {
  return request9527({
    url: `/Role/EditRoleUsers`,
    method: 'post',
    data
  })
}
// 根据角色获取模块按钮
export function GetModuleBtnRole(Id) {
  return request9527({
    url: `/Role/GetModuleBtnRole`,
    method: 'get',
    params: { Id }
  })
}
// 修改角色模块按钮
export function EditModuleBtnRole(data) {
  return request9527({
    url: `/Role/EditModuleBtnRole`,
    method: 'post',
    data
  })
}

export const btnMap = {
  'primary': 'primary',
  'success': 'success',
  'warning': 'warning',
  'danger': 'danger',
  'info': 'info',
  'text': '',
  'btn-primary': 'primary',
  'btn-success': 'success',
  'btn-warning': 'warning',
  'btn-danger': 'danger',
  'btn-info': 'info',
  'btn-default': ''
}

export function IconFilter(icon) {
  if (icon) {
    const iconStr = icon.split(' ')
    const iconFirst = iconStr[0]
    const iconSecond = iconStr[iconStr.length - 1].match(/fa-(\S*)/)
    if (!iconSecond) {
      return [iconFirst, iconStr[1]]
    }
    let iconContent = iconSecond[1]
    if (/-o/.test(iconContent)) {
      iconContent = (iconContent.match(/(\S*)-o/))[1]
    }
    if (iconContent === 'refresh') {
      iconContent = 'sync-alt'
    }
    if (iconContent === 'floppy') {
      iconContent = 'save'
    }
    if (iconContent === 'files') {
      iconContent = 'copy'
    }
    if (iconContent === 'cloud-download') {
      iconContent = 'cloud-download-alt'
    }
    return [iconFirst, iconContent]
  }
  return ['far', 'folder-open']
}
