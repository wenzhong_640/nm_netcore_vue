import request from '@/utils/request'
import request9527 from '@/utils/request9527'

export function login(data) {
  return request9527({
    url: `/Login/CasLogin`,
    method: 'get',
    params: { userId: data.username }
  })
}

export function ticketLogin(ticket, url) {
  return request9527({
    url: `/Login/CasLogin`,
    method: 'get',
    params: { ticket, url }
  })
}

export function getRoutes(userID) {
  return request9527({
    url: `/Module/GetUserModule`,
    method: 'get',
    params: { userId: userID }
  })
}

export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

