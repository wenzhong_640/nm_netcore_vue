import { asyncRoutes, constantRoutes } from '@/router'
import { getRoutes } from '@/api/user'
import Layout from '@/layout'
/**
 * 通过meta.role判断是否与当前用户权限匹配
 * @param roles
 * @param route
 */
function hasPermission(roles, route) {
  if (route.meta && route.meta.roles) {
    return roles.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * 递归过滤异步路由表，返回符合用户角色权限的路由表
 * @param routes asyncRoutes
 * @param roles
 */
function filterApiRoutes(routes) {
  const res = []
  routes.forEach(route => {
    const tmp = { ...route }
    if (!tmp.url) {
      return false
    }
    if (tmp.type === 1) {
      const path = tmp.url.split('/')[0] === '' ? tmp.url.split('/')[1] : tmp.url.split('/')[0]
      tmp.path = `/${path}`
      tmp.component = tmp.father ? Layout : () => import(`@/views${tmp.url}/index`)
      tmp.redirect = tmp.url
      tmp.name = path
      tmp.alwaysShow = true
      tmp.meta = {
        title: path,
        icon: 'table'
      }
    }

    if (tmp.type === 2) {
      const path = tmp.url.split('/')
      const index = path.length - 1
      tmp.path = path[index]
      tmp.component = () => import(`@/views${tmp.url}/index`)
      tmp.name = path[index]
      tmp.meta = {
        title: tmp.moduleName,
        id: tmp.id
      }
    }

    if (tmp.child && tmp.child.length > 0) {
      tmp.children = filterApiRoutes(tmp.child)
    }
    res.push(tmp)
  })
  return res
}

export function filterAsyncRoutes(routes, roles) {
  const res = []

  routes.forEach(route => {
    const tmp = { ...route }
    if (hasPermission(roles, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, roles)
      }
      res.push(tmp)
    }
  })
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  },
  getgenerateRoutes({ commit }, products) {
    return new Promise((resolve, reject) => {
      getRoutes(products.userID).then(response => {
        var setMockdata = []
        setMockdata = response.data
        let all = []
        if (setMockdata.length > 0) {
          setMockdata.forEach(route => {
            route.father = true
          })
          all = filterApiRoutes(setMockdata)
        }
        // const first = products.accessRoutes.slice(0, products.accessRoutes.length - 1)
        const end = products.accessRoutes && products.accessRoutes.length > 0 ? products.accessRoutes.slice(products.accessRoutes.length - 1, products.accessRoutes.length) : []
        const totalRoutes = [...all, ...end]
        commit('SET_ROUTES', totalRoutes)
        resolve(totalRoutes)
      }).catch(error => {
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
